// Tương tự câu lệnh import express from 'express'
const express = require('express');

// Khởi tạo app expressJs
const app = express();

// Khai báo cổng chạy API
const port = 8000;

// Khai báo để ứng dụng đọc được req body json
app.use(express.json());

app.get("/", (req, res) => {
    const today = new Date();

    res.json({
        message: `Xin chào, hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`
    })
});

app.get("/get-method", (req, res) => {
    res.json({
        message: "GET method"
    });
})

app.post("/post-method", (req, res) => {
    res.json({
        message: "POST method"
    });
})

app.put("/put-method", (req, res) => {
    res.json({
        message: "PUT method"
    });
})

app.delete("/delete-method", (req, res) => {
    res.json({
        message: "DELETE method"
    });
})

// REQ Params có thể dùng cho nhiều loại phương thức GET, POST, PUT, DELETE
// Req Params bắt buộc phải có trên URL
app.get("/request-params/param/:param1/:param", (req, res) => {
    let param1 = req.params.param1;
    let param = req.params.param;

    res.json({
        param,
        param1
    })
})

// REQ Query thường chỉ dùng cho phương thức GET
// VD: http://localhost:8000/request-query?key1=value1&key2=value2
// Nằm sau dấu ? của URL, các cặp key - value cách nhau bởi dấu &
// Không bắt buộc với URL (Nếu cần dùng Req Query cần validate dữ liệu)
app.get("/request-query", (req, res) => {
    let query = req.query;

    res.json({
        query
    })
})

// REQ Query thường chỉ dùng cho phương thức POST hoặc PUT
app.post("/request-body-json", (req, res) => {
    let body = req.body;

    res.json({
        body
    })
})

// Chạy app trên port đã khai báo
app.listen(port, () => {
    console.log("App đang chạy trên cổng: ", port);
});